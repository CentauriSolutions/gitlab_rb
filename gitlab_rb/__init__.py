"""Package for interacting with a Gitlab config file (gitlab/rb)."""


def _isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False


def python_2_ruby(input):
    """Convert a typed value from Python to Ruby."""
    input_value = str(input)
    if isinstance(input, bool):
        if (input):
            input_value = "true"
        else:
            input_value = "false"
    elif (isinstance(input, list)):
        input_value = '['
        inputs = [str(python_2_ruby(py)) for py in input]
        input_value += ', '.join(inputs)
        input_value += ']'
    elif (isinstance(input, dict)):
        input_value = '{'
        inputs = ["{} => {}".format(
            python_2_ruby(k), python_2_ruby(v)
        ) for k, v in input.items()]
        input_value += ', '.join(inputs)
        input_value += '}'
    elif (input_value.isdigit()):
        input_value = int(input_value)
    elif (_isfloat(input_value)):
        input_value = float(input_value)
    else:
        input_value = '\'' + input_value + '\''
    return input_value


def ruby_2_python(input):
    """Convert a typed value from Ruby to Python."""
    if input == 'true':
        return True
    elif input == 'false':
        return False
    elif (input.isdigit()):
        return int(input)
    elif (_isfloat(input)):
        return float(input)
    return input.strip("'")
