"""Module for interacting with a Gitlab config file (gitlab/rb)."""

from gitlab_rb import (
    python_2_ruby,
    ruby_2_python,
)


class GitlabRb(dict):
    """Representation of a gitlab.rb configuration file."""

    def __init__(self, config):
        """Create a new GitlabRb from the file's contents."""
        self._config = config.splitlines()

    def update(self, attribute, setting, section='gitlab_rails', bare=False):
        """Update the specified attribute with the given value."""
        key = str(attribute)
        if not bare:
            key = "{}['{}']".format(section, key)

        setting_value = python_2_ruby(setting)

        if bare:
            new_line = """{} {}""".format(key, setting_value)
        else:
            new_line = """{} = {}""".format(key, setting_value)
        for (id, line) in enumerate(self._config):
            if not bare:
                if '=' in line:
                    var = str(line.split('=')[0].rstrip(' '))
                else:
                    continue
            else:
                var = str(line.split(' ')[0].rstrip(' '))
            try:
                var = str(var.split('#')[1].strip(' '))
            except IndexError:
                pass
            if var == key:
                self._config[id] = new_line
                return
        self._config.append(new_line)

    def get(self, attribute, section='gitlab_rails', bare=False):
        """Get the specified attribute."""
        if not bare:
            attribute = "{}['{}']".format(section, attribute)
        for line in self._config:
            if not bare:
                if '=' in line:
                    attr = str(line.split('=')[0].rstrip(' '))
                else:
                    continue
            else:
                attr = str(line.split(' ')[0].rstrip(' '))
            if attr == attribute:
                if not bare:
                    split = '='
                else:
                    split = ' '
                setting = str(line.split(split)[1].lstrip(' ').rstrip())
                return ruby_2_python(setting)
        return None

    def __repr__(self):
        """Return Config as a string."""
        return "\n".join(self._config)
