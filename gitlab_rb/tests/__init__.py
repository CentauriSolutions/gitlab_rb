from unittest import TestCase

import gitlab_rb.config
from gitlab_rb import (
    python_2_ruby,
    ruby_2_python,
)


class TestGitlab(TestCase):
    def test_read_unconfigured_bare(self):
        config = gitlab_rb.config.GitlabRb("""
external_url 'https://gitlab.example.com'""")
        self.assertEqual(config.get('external_url'), None)

    def test_read_external_url(self):
        config = gitlab_rb.config.GitlabRb("""
external_url 'https://gitlab.example.com'""")
        self.assertEqual(config.get('external_url', bare=True),
                         'https://gitlab.example.com')

    def test_write_external_url(self):
        config = gitlab_rb.config.GitlabRb("""
external_url 'https://gitlab.example.com'""")
        config.update('external_url', 'https://example.com', bare=True)
        self.assertEqual(config.get('external_url', bare=True),
                         'https://example.com')

    def test_read_config(self):
        config = gitlab_rb.config.GitlabRb("""
external_url 'https://gitlab.example.com'
gitlab_rails['gitlab_email_enabled'] = true""")
        self.assertEqual(config.get('gitlab_email_enabled'), True)

    def test_update_config(self):
        config = gitlab_rb.config.GitlabRb("""
external_url 'https://gitlab.example.com'
gitlab_rails['gitlab_email_enabled'] = true""")

        self.assertEqual(config.get('gitlab_email_enabled'), True)
        config.update('gitlab_email_enabled', False)
        self.assertEqual(config.get('gitlab_email_enabled'), False)

    def test_update_int(self):
        config = gitlab_rb.config.GitlabRb("""
external_url 'https://gitlab.example.com'
# gitlab_rails['gitlab_default_theme'] = 2""")
        config.update('gitlab_default_theme', 2)
        self.assertEqual(config.get('gitlab_default_theme'), 2)

    def test_update_float(self):
        config = gitlab_rb.config.GitlabRb("""
external_url 'https://gitlab.example.com'""")
        config.update('gitlab_float_config', 2.2)
        self.assertEqual(config.get('gitlab_float_config'), 2.2)

    def test_new_config(self):
        config = gitlab_rb.config.GitlabRb("""
external_url 'https://gitlab.example.com'
gitlab_rails['gitlab_email_enabled'] = true""")

        self.assertEqual(config.get('gitlab_email_reply_to'), None)
        config.update('gitlab_email_reply_to', 'noreply@example.com')
        self.assertEqual(config.get('gitlab_email_reply_to'),
                         'noreply@example.com')

    def test_updates_comment(self):
        config = gitlab_rb.config.GitlabRb("""
external_url 'https://gitlab.example.com'
# gitlab_rails['gitlab_email_enabled'] = true""")
        self.assertEqual(config.get('gitlab_email_enabled'), None)
        print("Config: '{}'".format(config))
        config.update('gitlab_email_enabled', True)
        self.assertEqual(config.get('gitlab_email_enabled'), True)
        print("Config: '{}'".format(config))

        self.assertEqual(str(config), """
external_url 'https://gitlab.example.com'
gitlab_rails['gitlab_email_enabled'] = true""")

    def test_a_float(self):
        self.assertTrue(gitlab_rb._isfloat("123.456"))

    def test_not_a_float(self):
        self.assertFalse(gitlab_rb._isfloat("comething else"))


class TestPython2Ruby(TestCase):
    def test_bool_true(self):
        self.assertEqual(python_2_ruby(True), 'true')

    def test_bool_false(self):
        self.assertEqual(python_2_ruby(False), 'false')

    def test_list_empty(self):
        self.assertEqual(python_2_ruby([]), '[]')

    def test_list(self):
        self.assertEqual(python_2_ruby([1, 3.14, 'test']), "[1, 3.14, 'test']")

    def test_dict_empty(self):
        self.assertEqual(python_2_ruby({}), '{}')

    def test_dict(self):
        self.assertEqual(python_2_ruby({'a': 3.14, 'b': 'test'}),
                         "{'a' => 3.14, 'b' => 'test'}")

    def test_integer(self):
        self.assertEqual(python_2_ruby(2), 2)

    def test_float(self):
        self.assertEqual(python_2_ruby(3.14), 3.14)

    def test_str(self):
        self.assertEqual(python_2_ruby('stuff'), "'stuff'")


class TestRuby2Python(TestCase):
    def test_bool_true(self):
        self.assertEqual(ruby_2_python('true'), True)

    def test_bool_false(self):
        self.assertEqual(ruby_2_python('false'), False)

    def test_list_empty(self):
        pass

    def test_list(self):
        pass

    def test_dict_empty(self):
        pass

    def test_dict(self):
        pass

    def test_integer(self):
        self.assertEqual(ruby_2_python('2'), 2)

    def test_float(self):
        self.assertEqual(ruby_2_python('3.14'), 3.14)

    def test_str(self):
        self.assertEqual('stuff', ruby_2_python('stuff'))
