# Gitlab_rb

[![codecov](https://codecov.io/gl/CentauriSolutions/gitlab_rb/branch/master/graph/badge.svg)](https://codecov.io/gl/CentauriSolutions/gitlab_rb)

This is a python library designed to help write Gitlab's config file (/etc/gitlab/gitlab.rb) from Python.