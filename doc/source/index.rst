.. gitlab_rb documentation amster file

Welcome to Gitlab_rb's documentation
====================================

Contents:

.. toctree::
   :maxdepth: 2

   config

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`