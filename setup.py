from setuptools import setup

setup(name='gitlab_rb',
      version='0.1',
      description="Python library to configure Gitlab's config file",
      url='https://gitlab.com/centaurisolutions/gitlab_rb',
      author='Chris MacNaughton',
      author_email='gitlab_rb@chris.centauri.solutions',
      license='MIT',
      packages=['gitlab_rb'],
      zip_safe=False)